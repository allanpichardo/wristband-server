<?php

class Comment{
    private $bandId;
    
    public function __construct($bandId) {
        $this->bandId = $bandId;
    }
    
    public function addComment($alias,$body) {

        $outString = API::encode(false, null, "Unknown Error");

        $db = new Database();
        
        $alias = $db->getLink()->real_escape_string($alias);
        $body = $db->getLink()->real_escape_string($body);
        
        $remoteIp = $_SERVER['REMOTE_ADDR'];
        $forwardIp = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        $query = "insert into comments (band_id,remote_ip,forward_ip,alias,body) values ('$this->bandId','$remoteIp','$forwardIp','$alias','$body')";
        $result = $db->query($query);
        if ($result) {
            $data = new stdClass();
            $data->comments = $this->getComments();
            $outString = API::encode(true, $data);
        } else {
            $outString = API::encode(false, null, $db->getLink()->error);
        }

        return $outString;
    }
    
    public function getCount() {
        $db = new Database();

        $query = "select count(id) as count from comments where band_id = '$this->bandId'";
        $result = $db->query($query);

        $count = 0;
        while ($row = $result->fetch_object()) {
            $count = $row->count;
        }

        return $count;
    }
    
    public function getComments(){
        $db = new Database();
        
        $query = "select * from comments where band_id = '$this->bandId'";
        $result = $db->query($query);
        
        $comments = array();
        while($comment = $result->fetch_object()){
            array_push($comments, $comment);
        }
        
        return $comments;
    }
}