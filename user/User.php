<?php 
class User{
	
	private $error = false;
	
	private $authenticated = false;
	private $userId;
	private $username;
	private $hash;
	private $email;
	
	/**
	 * Construct a User object with known user id
	 * @param integer $userId
	 */
	public function __construct($userId){
		
		$db = new Database();
		
		$query = "select * from users where id = %s";
		$query = sprintf($query,$userId);
		$result = $db->query($query);
		
		if($result->num_rows > 0){
			$user = $result->fetch_object();
			
			$this->userId = $user->id;
			$this->username = $user->username;
			$this->hash = $user->password;
			$this->email = $user->email;
		}else{
			$this->error = $db->getLink()->error;
		}
		
	}
	
	public function getError(){
		return $this->error;
	}
	
	/**
	 * Creates a new user in the system.
	 * @param string $userName
	 * @param string $email
	 * @param string $password
	 * @return string JSON response
	 */
	public static function createNew($userName, $email, $password){
		
		$outString = API::encode(false, null,"Unknown Error");
		
		$db = new Database();
		
		$userName = $db->getLink()->real_escape_string($userName);
		$email = $db->getLink()->real_escape_string($email);
		$pwHash = password_hash($password,PASSWORD_DEFAULT);
		
		$query = "insert into users (username,email,password) values ('%s','%s','%s')";
		$query = sprintf($query,$userName,$email,$pwHash);
		$result = $db->query($query);
		
		if($result){
			$id = $db->getLink()->insert_id;
			$result = $db->query("select * from users where id = $id");
			$user = $result->fetch_object();
			$outString = API::encode(true, $user);
		}else{
			$outString = API::encode(false, null, $db->getLink()->error);
		}
		
		return $outString;
	}
	
	/**
	 * Verify a user's credentials and find a user
	 * @param string $username
	 * @param string $password
	 * @return JSON response
	 */
	public static function authenticate($username, $password){
		
		$outString = API::encode(false, null,"Unknown Error");
		
		$db = new Database();
		
		$query = "select * from users where username = '%s'";
		$query = sprintf($query,$username);
		$result = $db->query($query);
		if($result->num_rows > 0){
			$user = $result->fetch_object();

			//verify the password
			$isVerified = password_verify($password,$user->password);
			
			if($isVerified){
				$outString = API::encode(true, $user);
			}else{
				$outString = API::encode(false, null,"Incorrect password");
			}
			
		}else{
			$outString = API::encode(false, null,"Could not find user $username");
		}
		
		return $outString;
	}
	
	public function getUserId(){
		return $this->userId;
	}
	
	public function getUsername(){
		return $this->username;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	/**
	 * Get formatted JSON data pertaining to this user
	 * @return string
	 */
	public function getData(){
		$db = new Database();
		
		$query = "select * from users where id = $this->userId";
		$result = $db->query($query);
		
		if($result->num_rows > 0){
			$user = $result->fetch_object();
			$favorite = new Favorite($this->userId);
			$user->favorites = $favorite->getArray();
			return API::encode(true, $user);
		}else{
			return API::encode(false, null,$db->getLink()->error);
		}
	}
	
	public function update($json){
		
		$outString = API::encode(false, null,"Unknown Error");
		
		$db = new Database();
		
		$json = json_decode($json);
		$properties = get_object_vars($json);
		
		$query = "update users set ";
		$numItems = count($properties);
		$i = 0;
		foreach ($properties as $property){
			if(key($properties) == 'password'){
				$property = password_hash($property,PASSWORD_DEFAULT);
			}else{
				$property = $db->getLink()->real_escape_string($property);
			}
			$query .= sprintf("%s = '%s'",key($properties),$property);
			if(++$i === $numItems){
				$query .= " ";
			}else{
				$query .= ", ";
			}
		}
		$query .= "where id = $this->userId";
		$result = $db->query($query);
		if($result){
			$outString = $this->getData();
		}else{
			$outString = API::encode(false, null,$db->getLink()->error);
		}
		
		return $outString;
	}
}
?>