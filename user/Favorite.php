<?php

class Favorite {

    private $bandId;

    public function __construct($bandId) {
        $this->bandId = $bandId;
    }

    public function incrementFavorite() {

        $outString = API::encode(false, null, "Unknown Error");

        $db = new Database();
        $remoteIp = $_SERVER['REMOTE_ADDR'];
        $forwardIp = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        $query = "insert into favorites (band_id,remote_ip,forward_ip) values ('$this->bandId','$remoteIp','$forwardIp')";
        $result = $db->query($query);
        if ($result) {
            $data = new stdClass();
            $data->favorite_count = $this->getCount();
            $outString = API::encode(true, $data);
        } else {
            $outString = API::encode(false, null, $db->getLink()->error);
        }

        return $outString;
    }

    public function getCount() {
        $db = new Database();

        $query = "select count(id) as count from favorites where band_id = '$this->bandId'";
        $result = $db->query($query);

        $count = 0;
        while ($row = $result->fetch_object()) {
            $count = $row->count;
        }

        return $count;
    }

}

?>