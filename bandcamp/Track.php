<?php
class Track{
	
	private $json;
	
	public function __construct($json){
		$this->json = $json;
	}
	
	public function getBandId(){
		return $this->json->band_id;
	}
	
	public function getNumber(){
		return $this->json->number;
	}
	
	public function getLyrics(){
		return $this->json->lyrics;
	}
	
	public function getStreamingUrl(){
		return $this->json->streaming_url;
	}
	
	public function getTrackId(){
		return $this->json->track_id;
	}
	
	public function getAbout(){
		return $this->json->about;
	}
	
	public function getUrl(){
		return $this->json->url;
	}
	
	public function getTitle(){
		return $this->json->title;
	}
	
	public function getDuration(){
		return $this->json->duration;
	}
	
	public function getCredits(){
		return $this->json->credits;
	}
	
	public function getAlbumId(){
		return $this->json->album_id;
	}
}
?>