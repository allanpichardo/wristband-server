<?php

class Bandcamp {

	public static $API_KEY = "baugihungrmordaafstanda";
	public static $API_KEY_bc = "vatnajokull";

	private $bandName;
	private $bandId;
	private $bandcampUrl;
	private $offsiteUrl;

	private $isEmpty;

	private $discography;

	public function __construct($bandName) {
		$this -> bandName = $bandName;
		$found = $this -> load($bandName);
		$this -> isEmpty = !$found;

		if ($found) {
			$this -> discography = new Discography($this -> bandId);
		}
	}

	public function isEmpty() {
		return $this -> isEmpty;
	}

	/**
	 * returns bandcamp id of band
	 */
	private function load($bandName) {

		$execution = false;

		//first search db
		$database = new Database();
		//do we have have this in the ignore table?
		if (!$database -> isIgnored($bandName)) {
			//we don't. let's check the cache
			$result = $database -> getBandcampCache($bandName);
			if ($result != false && $result -> num_rows > 0) {
				//found one, return this
				$row = $result -> fetch_object();
				$this -> bandId = $row -> band_id;
				$this -> bandcampUrl = $row -> bandcamp_url;
				$this -> offsiteUrl = $row -> offsite_url;
				
				$execution = true;
			} else {
				//need to fetch it from bandcamp, store it and then return it
				$url = sprintf("http://api.bandcamp.com/api/band/3/search?key=%s&name=%s", Bandcamp::$API_KEY, urlencode($bandName));
				$curl = curl_init();
				$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
				curl_setopt($curl, CURLOPT_USERAGENT, $agent);
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_ENCODING ,"");
				curl_setopt($curl, CURLOPT_VERBOSE, true);
				curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true);
				curl_setopt($curl,CURLOPT_MAXREDIRS,50);
				curl_setopt($curl, CURLOPT_TIMEOUT, 3);
				$res = curl_exec($curl);
				curl_close($curl);

				$json = json_decode($res);
				$results = $json -> results;
				if (count($results) > 0) {
					//get first element, insert it into DB and then return
					$band = $results[0];
					$query = sprintf("insert into bandcamp (band_id,name,bandcamp_url,offsite_url) values ('%s','%s','%s','%s')", $band -> band_id, $band -> name, $band -> url, $band -> offsite_url);
					$result = null;
					$result = $database -> query($query);
					if ($result != false) {
						$this -> bandId = $band -> band_id;
						$this -> bandcampUrl = $band -> url;
						$this -> offsiteUrl = $band -> offsite_url;

						$execution = true;
					}
				} else {
					//record that it doesn't exist
					//so that we don't have to search again
					$database -> ignore($bandName);
				}
			}
		}

		return $execution;
	}

	public function getDiscography() {
		return $this -> discography;
	}

	public function getBandcampUrl() {
		return $this -> bandcampUrl;
	}

	public function getBandId() {
		return $this -> bandId;
	}

	public function getBandName() {
		return $this -> bandName;
	}

	public function getOffsiteUrl() {
		return $this -> offsiteUrl;
	}

}
?>