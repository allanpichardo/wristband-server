<?php

class Discography
{

    private $albumMeta;
    private $album;

    public function __construct($bandId)
    {

        //check if we have a dicography cached
        $database = new Database();
        $result = $database->getDiscography($bandId);
        if ($result != false && $result->num_rows > 0) {
            //convert the album meta to json
            $json = array();
            while ($row = $result->fetch_object()) {
                array_push($json, $row);
            }
            $this->albumMeta = $json;
        } else {
            //get from bandcamp
            $url = sprintf("http://api.bandcamp.com/api/band/3/discography?key=%s&band_id=%s", Bandcamp::$API_KEY, $bandId);
            $curl = curl_init();
            $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
            curl_setopt($curl, CURLOPT_USERAGENT, $agent);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_ENCODING ,"");
            curl_setopt($curl, CURLOPT_VERBOSE, true);
            curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true);
            curl_setopt($curl,CURLOPT_MAXREDIRS,50);
            curl_setopt($curl, CURLOPT_TIMEOUT, 3);
            $res = curl_exec($curl);
            curl_close($curl);
            $this->albumMeta = json_decode($res)->discography;

            //cache the discography
            $database->addDiscography($this->albumMeta);
        }
        //finally load the albums
        $this->loadAlbums();
    }

    private function loadAlbums()
    {
        try {
            for($i = 0; $i < count($this->albumMeta); ++$i){
                $info = $this->albumMeta[$i];
                if (!empty($info->album_id) && !empty($info->large_art_url)) {
                    $url = sprintf("http://api.bandcamp.com/api/album/2/info?key=%s&album_id=%s", Bandcamp::$API_KEY, $info->album_id);
                    $curl = curl_init();
                    $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
                    curl_setopt($curl, CURLOPT_USERAGENT, $agent);
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_ENCODING ,"");
                    curl_setopt($curl, CURLOPT_VERBOSE, true);
                    curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true);
                    curl_setopt($curl,CURLOPT_MAXREDIRS,50);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 3);
                    $res = curl_exec($curl);
                    curl_close($curl);
                    if ($res != null) {
                        $json = json_decode($res);
                        $album = new Album($json);
                        if (!empty($album->getTracks())) {
                            $this->album = $album;
                            break;
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->album = null;
        }
    }

    public function hasAlbum()
    {
        return $this->album != null;
    }

    public function getRandomAlbum()
    {
        return $this->album;
    }

}

?>