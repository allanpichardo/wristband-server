<?php
class Album{
	
	private $json;
	
	public function __construct($json){
		return $this->json = $json;
	}
	
	public function getBandId(){
		return $this->json->band_id;
	}
	
	public function getAbout(){
		return $this->json->about;
	}
	
	public function getSmallArtUrl(){
		return $this->json->small_art_url;
	}
	
	public function getUrl(){
		return $this->json->url;
	}
	
	public function getTitle(){
		return $this->json->title;
	}
	
	public function getCredits(){
		return $this->json->credits;
	}
	
	public function getLargeArtUrl(){
		return $this->json->large_art_url;
	}
	
	public function getAlbumId(){
		return $this->json->album_id;
	}
	
	public function getReleaseDate(){
		return $this->json->release_date;
	}
	
	/**
	 * Returns array of Track objects
	 */
	public function getTracks(){
		$array = array();

		foreach($this->json->tracks as $trackJson){
			$track = new Track($trackJson);
			array_push($array,$track);
		}
		
		return $array;
	}
	
	public function getRandomTrack(){
		$index = array_rand($this->json->tracks);
		$trackJson = $this->json->tracks[$index];
		$track = new Track($trackJson);
		return $track;
	}
}
?>