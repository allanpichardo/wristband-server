<?php

header("Access-Control-Allow-Origin: *");

if (true) {
	error_reporting(E_ALL);
}

//includes
include_once ('includes.php');

//variables
$fn = $_REQUEST['fn'];
$latitude = $_REQUEST['latitude'];
$longitude = $_REQUEST['longitude'];
$date = $_REQUEST['date'];
$bandName = isset($_REQUEST['band_name']) ? $_REQUEST['band_name'] : "";
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$bandId = isset($_REQUEST['band_id']) ? $_REQUEST['band_id'] : "";
$alias = isset($_REQUEST['alias']) ? $_REQUEST['alias'] : "";
$body = isset($_REQUEST['body']) ? $_REQUEST['body'] : "";

/**
 * For debug purposes, can do a var_dump of all events
 */

if ($fn == "dump_events") {
	$songkick = new Songkick($latitude, $longitude, $date);
	echo "<pre>";
	var_dump($songkick -> getEvents($page));
	echo "</pre>";
}
else if ($fn == "dump_band") {
	$bandcamp = new Bandcamp($bandName);
	echo "<pre>";
	var_dump($bandcamp);
	echo "</pre>";
}
else if ($fn == "get_shows") {
	printf("%s", getShows($latitude, $longitude, $date, $page));
}
else if ($fn == "add_favorite"){
    printf("%s", addFavorite($bandId));
}
else if ($fn == "add_comment"){
    printf("%s", addComment($bandId, $alias, $body));
}

function addComment($bandId, $alias, $body){
    $comment = new Comment($bandId);
    return $comment->addComment($alias, $body);
}

function addFavorite($bandId){
    $favorite = new Favorite($bandId);
    return $favorite->incrementFavorite();
}

function getShows($lat, $lng, $date, $page) {

	$timeStart = microtime(true);

	$result = new stdClass();
	$result -> shows = array();
	$result -> empty_bandcamps = 0;
	$result -> found_bandcamps = 0;

	$songkick = new Songkick($lat, $lng, $date);

	$existingShows = $songkick->getExistingEvents($page);
	
	$result -> execution = ($existingShows != null && count($existingShows) > 0);
	
	if($result->execution){
		$result->shows = $existingShows;
	} else {
		$metroId = $songkick->getMetroId();
		$db = new Database();

		$db->query("insert into metros (metro_id) values ($metroId)");

		$events = $songkick->getAllEventsByMetro($metroId);
		foreach($events as $event) {
			$songkick->saveEvent($event, $date, $metroId);
		}

		$result->execution = ($events != null && count($events) > 0);
		$result->shows = $songkick->getExistingEvents($page);
	}

	$timeEnd = microtime(true);
	$elapsed = $timeEnd - $timeStart;
	$result->execution_time = sprintf("%s seconds",$elapsed);

	$result = json_encode($result);
	return $result;
}
?>
