<?php


class EventResults{
	
	private $results;
	private $totalEntries;
	private $perPage;
	
	/**
	 * Parses the JSON results from a SongKick event search
	 */
	public function __construct($results){
		$json = json_decode($results);
		$this->totalEntries = $json->resultsPage->totalEntries;
		$this->perPage = $json->resultsPage->perPage;
		$this->results = $json->resultsPage->results->event;
	}
	
	/**
	 * Returns an array of Event objects
	 */
	public function getEvents(){
		$array = array();
		
		foreach ($this->results as $eventJson) {
			$event = new Event($eventJson);
			array_push($array,$event);
		}
		
		return $array;
	}
	
	public function getTotalEntries(){
		return $this->totalEntries;
	}
	
	public function getPerPage(){
		return $this->perPage;
	}
	
	public function getNumberPages(){
		return $this->getTotalEntries() / $this->getPerPage();
	}
}
?>