<?php

class Performance{
	
	private $json;
	
	public function __construct($json){
		$this->json = $json;
	}
	
	private function getArtist(){
		return $this->json->artist;
	}
	
	public function getArtistUrl(){
		return $this->getArtist()->uri;
	}
	
	public function getArtistName(){
		return $this->getArtist()->displayName;
	}
	
	public function getArtistId(){
		return $this->getArtist()->id;
	}
	
	public function getName(){
		return $this->json->displayName;
	}
	
	public function getBillingIndex(){
		return $this->json->billingIndex;
	}
	
	public function getId(){
		return $this->json->id;
	}
	
	public function getBilling(){
		return $this->billing;
	}
}
?>