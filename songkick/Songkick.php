<?php

class Songkick {

    public static $RESULTS_PER_PAGE = 15;
    public static $API_KEY = "plPE4X1rCdADL3A3";
    private $lat;
    private $lng;
    private $date;

    public function __construct($lat, $lng, $date) {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->date = $date;
    }

    public function getMetroId() {
        $url = sprintf("http://api.songkick.com/api/3.0/search/locations.json?location=geo:%s,%s&apikey=%s", $this->lat, $this->lng, Songkick::$API_KEY);
        $res = file_get_contents($url);

        $json = json_decode($res);

        //add or update the metros table
        $metro = $json->resultsPage->results->location[0]->metroArea;
        $metroId = $metro->id;
        $metroName = $metro->displayName;

        /* $query = "insert into metros (metro_id, name) values ('%s','%s') on duplicate key update name = '%s'";
          $query = sprintf($query,$metroId,$metroName,$metroName);

          $db = new Database();
          $db->query($query); */

        return $metroId;
    }

    public function getExistingEvents($page = 1) {
        $limitTotal = 12;
        $limitStart = ($page - 1) * $limitTotal;

        $query = "select * from shows where metro_id = '%s' and poll_date = '%s' order by venue_name limit %s,%s";
        $query = sprintf($query, $this->getMetroId(), $this->date, $limitStart, $limitTotal);

        $db = new Database();
        $result = $db->query($query);

        if ($result->num_rows > 0) {
            $shows = array();
            while ($show = $result->fetch_object()) {
                $favorite = new Favorite($show->band_id);
                $comment = new Comment($show->band_id);
                $show->favorite_count = $favorite->getCount();
                $show->comment_count = $comment->getCount();
                $show->comments = $comment->getComments();
                $show->poll_page = $page;
                array_push($shows, $show);
            }
            return $shows;
        } else {
            return null;
        }
    }

    public function saveEvent($event, $date, $metroId) {
        $db = new Database();

        foreach ($event->getPerformances() as $performance) {
            $bandcamp = new Bandcamp($performance->getName());
            if (!$bandcamp->isEmpty()) {
                try {
                    $json = new stdClass();
                    //get the concert info
                    $json->poll_page = 0;
                    $json->poll_date = $db->getLink()->escape_string($date);
                    $json->venue_name = $db->getLink()->escape_string($event->getVenueName());
                    $json->venue_lat = $event->getVenueLatitude();
                    $json->venue_lng = $event->getVenueLongitude();
                    $json->datetime = $db->getLink()->escape_string($event->getStartDatetime());
                    $json->songkick_url = $db->getLink()->escape_string($event->getEventUrl());
                    //get band info
                    $json->band_name = $db->getLink()->escape_string($bandcamp->getBandName());
                    $json->bandcamp_url = $db->getLink()->escape_string($bandcamp->getBandcampUrl());
                    $json->alternate_url = $db->getLink()->escape_string($bandcamp->getOffsiteUrl());
                    $json->band_id = $db->getLink()->escape_string($bandcamp->getBandId());
                    //get random album
                    $discography = $bandcamp->getDiscography();
                    if ($discography != null && $discography->hasAlbum()) {
                        $album = $discography->getRandomAlbum();
                        //get album info
                        $json->small_art_url = $db->getLink()->escape_string($album->getSmallArtUrl());
                        $json->large_art_url = $db->getLink()->escape_string($album->getLargeArtUrl());
                        $json->album_title = $db->getLink()->escape_string($album->getTitle());
                        $json->album_url = $db->getLink()->escape_string($album->getUrl());
                        //get random track
                        $track = $album->getRandomTrack();
                        //get track info
                        $json->track_title = $db->getLink()->escape_string($track->getTitle());
                        $json->track_streaming_url = $db->getLink()->escape_string($track->getStreamingUrl());
                        $json->track_url = $db->getLink()->escape_string($track->getUrl());

                        //insert everything into the database
                        $query = "INSERT INTO shows (poll_page,poll_date,venue_name,venue_lat,venue_lng," .
                            "datetime,songkick_url,band_name,bandcamp_url,alternate_url,band_id," .
                            "small_art_url,large_art_url,album_title,album_url,track_title," .
                            "track_streaming_url,track_url,metro_id) VALUES ('%s','%s','%s',%s,%s," .
                            "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s') " .
                            "ON DUPLICATE KEY UPDATE band_name = '%s'";
                        $query = sprintf($query,
                            $json->poll_page, $json->poll_date, $json->venue_name, $json->venue_lat, $json->venue_lng,
                            $json->datetime, $json->songkick_url, $json->band_name, $json->bandcamp_url, $json->alternate_url, $json->band_id,
                            $json->small_art_url, $json->large_art_url, $json->album_title, $json->album_url, $json->track_title,
                            $json->track_streaming_url, $json->track_url, $metroId, $json->band_name);
                        $queryResult = $db->query($query);
                    }else{
                        continue;
                    }
                } catch (Exception $e) {

                }
            }
        }
    }

    public function getEvents($page = 1) {
        $url = sprintf("http://api.songkick.com/api/3.0/metro_areas/%s/calendar.json?apikey=%s&min_date=%s&max_date=%s&page=%s&per_page=%s", $this->getMetroId(), Songkick::$API_KEY, $this->date, $this->date, $page, Songkick::$RESULTS_PER_PAGE);
        /* $url = sprintf("http://api.songkick.com/api/3.0/events.json?apikey=%s&location=geo:%s,%s&min_date=%s&max_date=%s&page=%s&per_page=%s",
          Songkick::$API_KEY,$this->lat,$this->lng,$this->date,$this->date,$page,Songkick::$RESULTS_PER_PAGE); */
        $res = file_get_contents($url);

        $results = new EventResults($res);

        return $results->getEvents();
    }

    public function getAllEventsByMetro($metroId) {

        $allEvents = array();

        $url = sprintf("http://api.songkick.com/api/3.0/metro_areas/%s/calendar.json?apikey=%s&min_date=%s&max_date=%s&page=%s&per_page=%s", $metroId, Songkick::$API_KEY, $this->date, $this->date, 1, 100);
        $res = file_get_contents($url);
        $results = new EventResults($res);

        foreach ($results->getEvents() as $event) {
            array_push($allEvents, $event);
        }

        for ($i = 2; $i < $results->getNumberPages();  ++$i) {
            $url = sprintf("http://api.songkick.com/api/3.0/metro_areas/%s/calendar.json?apikey=%s&min_date=%s&max_date=%s&page=%s&per_page=%s", $metroId, Songkick::$API_KEY, $this->date, $this->date, $i, 100);
            $res = file_get_contents($url);
            $results = new EventResults($res);

            foreach ($results->getEvents() as $event) {
                array_push($allEvents, $event);
            }
        }

        return $allEvents;
    }

}

?>
