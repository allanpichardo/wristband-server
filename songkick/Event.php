<?php

class Event{
	
	private $json;
	
	public function __construct($json){
		$this->json = $json;
	}
	
	public function getId(){
		return $this->json->id;
	}
	
	public function getEventName(){
		return $this->json->displayName;
	}
	
	public function getEventType(){
		return $this->json->type;
	}
	
	public function getEventUrl(){
		return $this->json->uri;
	}
	
	private function getVenue(){
		return $this->json->venue;
	}
	
	public function getVenueLatitude(){
		return $this->getVenue()->lat;
	}
	
	public function getVenueLongitude(){
		return $this->getVenue()->lng;
	}
	
	public function getVenueName(){
		return $this->getVenue()->displayName;
	}
	
	public function getVenueId(){
		return $this->getVenue()->id;
	}
	
	private function getLocation(){
		return $this->json->location;
	}
	
	public function getCity(){
		return $this->getLocation()->city;
	}
	
	private function getStart(){
		return $this->json->start;
	}
	
	public function getStartDatetime(){
		return $this->getStart()->datetime;
	}
	
	public function getPerformances(){
		$array = array();
		
		foreach($this->json->performance as $performanceJson){
			$performance = new Performance($performanceJson);
			array_push($array,$performance);
		}
		
		return $array;
	}
}
?>