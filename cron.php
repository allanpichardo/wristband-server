<?php
if (true) {
    error_reporting(E_ALL);
}

//includes
include_once('includes.php');

//get tonight's shows
function aggregateShows()
{
    $manualDate = getopt("d:");
    if(!empty($manualDate)) {
        $manualDate = $manualDate['d'];
        echo "PROCEEDING WITH MANUAL DATE $manualDate";
    }

    $db = new Database();

    $timeStart = microtime(true);

    $date = !empty($manualDate) ? $manualDate : date("Y-m-d");

    echo "\nFetching Metro ID's on $date\n";

    $songkick = new Songkick(0, 0, $date);

    $query = "SELECT * FROM metros";
    $metroResult = $db->query($query);

    while ($metro = $metroResult->fetch_object()) {

        echo "\nMetro $metro->metro_id \n";

        $events = $songkick->getAllEventsByMetro($metro->metro_id);

        if (count($events) == 0) {
            echo "no shows\n";
            return;
        } else {
            echo "Found " . count($events) . " events for $metro->city...\n";
        }

        foreach ($events as $event) {
            echo "***";
            $songkick->saveEvent($event, $date, $metro->metro_id);
        }
    }

    $timeEnd = microtime(true);
    $elapsed = $timeEnd - $timeStart;

    echo "\n Finished in $elapsed seconds";

}

aggregateShows();

?>