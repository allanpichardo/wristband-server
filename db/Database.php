<?php
class Database {

	private $mysqli;
	private $connectionError;

	public function __construct() {
		try {
                        $json = file_get_contents(dirname( __FILE__ ) . '/keys.json');
                        $keys = json_decode($json);
			$this -> mysqli = new mysqli($keys->db_host, $keys->db_user, $keys->db_password, $keys->db_name);
			//perform cleaning
			$this -> cleanCache();
		} catch(Exception $e) {
			$this -> connectionError = $e -> getMessage();
		}
	}

	public function cleanCache() {
		//clean ignore table for entries older than 30 days
		$cleanQuery = "delete from ignore_band where timestamp < (now() - interval %s month)";
		$cleanQuery = sprintf($cleanQuery, 1);

		//clean discography that is older than 3 months
		$discoQuery = "delete from discography where timestamp < (now() - interval %s month)";
		$discoQuery = sprintf($discoQuery, 3);

		$this -> query($cleanQuery);
		$this -> query($discoQuery);
	}

	public function query($query) {
		$result = $this -> mysqli -> query($query);
		return $result;
	}

	public function getConnectionError() {
		return $this -> connectionError;
	}

	public function ignore($bandName) {
		$execution = false;

		$query = "insert into ignore_band (band_name) values ('%s') on duplicate key update id = id";
		$query = sprintf($query, $bandName);
		$result = $this -> query($query);
		if ($result) {
			$execution = true;
		}

		return $execution;
	}

	public function isIgnored($bandName) {
		$ignored = false;

		$query = "select * from ignore_band where band_name = '%s'";
		$query = sprintf($query, $bandName);
		$result = $this -> query($query);
		if ($result != false && $result -> num_rows > 0) {
			$ignored = true;
		}

		return $ignored;
	}

	public function getBandcampCache($bandName) {
		$query = sprintf("select * from bandcamp where name = '%s'", $bandName);
		$result = $this -> query($query);
		return $result;
	}

	public function addDiscography($albumMeta) {
		if($albumMeta != null) {
			foreach ($albumMeta as $meta) {
				if (isset($meta->album_id)) {

					$query = "INSERT INTO discography (large_art_url,title,downloadable,album_id,artist,release_date,art_id,band_id,url,small_art_url) " . "VALUES ('%s','%s',%s,%s,'%s',%s,%s,'%s','%s','%s')";
					$query = sprintf($query, $meta->large_art_url, $meta->title, $meta->downloadable, $meta->album_id, $meta->artist, $meta->release_date, $meta->art_id, $meta->band_id, $meta->url, $meta->small_art_url);

					$result = $this->query($query);
				}
			}
		}
	}
	
	public function getLink(){
		return $this->mysqli;
	}

	public function getDiscography($bandId) {

		$query = "select * from discography where band_id = '%s'";
		$query = sprintf($query, $bandId);

		$result = $this -> query($query);

		return $result;
	}

}
?>