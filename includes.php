<?php

if (true) {
    error_reporting(E_ALL);
}

require_once(__DIR__.'/db/Database.php');
require_once(__DIR__.'/songkick/Songkick.php');
require_once(__DIR__.'/songkick/Event.php');
require_once(__DIR__.'/songkick/EventResults.php');
require_once(__DIR__.'/songkick/Performance.php');

require_once(__DIR__.'/bandcamp/Bandcamp.php');
require_once(__DIR__.'/bandcamp/Album.php');
require_once(__DIR__.'/bandcamp/Discography.php');
require_once(__DIR__.'/bandcamp/Track.php');

require_once(__DIR__.'/user/User.php');
require_once(__DIR__.'/user/Favorite.php');
require_once(__DIR__.'/user/Comment.php');

require_once(__DIR__.'/api/API.php');

?>