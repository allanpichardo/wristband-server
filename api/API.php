<?php 
/**
 * API return formatter
 */

class API{

	public static function encode($execution, $object, $error = ''){
		$return = new stdClass();
		$return->execution = $execution;
		$return->data = $object;
		$return->error = $error;
		return json_encode($return);
	}

	public static function reportErrors($reportOn){
		if($reportOn){
			ini_set('display_errors',1);
			error_reporting(E_ALL);
		}else{
			ini_set('display_errors',0);
			error_reporting(0);
		}
	}

}

?>
